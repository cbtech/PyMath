#!/usr/bin/python3

import os
import pygame
from randomNumber import Calc
import random
from pygame.locals import *
import pygame.font
import sys
import pdb

class Game:
    def __init__(self,screen):
        self.initial = Rect(70,390,220,166)
        self.my_screen = screen
        self.cursor_img = self.load_image('resources/Asset.png') 
        self.cursor_x = [70,290,513,735]
        self.cursor_pos = 0
        self.case_list = []
        self.case_rect_x = [135,350,575,790]
        self.card_result = [0,0,0,0]
        self.selected_place_x = random.randint(0,3)
        self.text_color = (66,66,66)
        self.font = pygame.font.SysFont(None, 60)
        self.result = 0
        self.max_calc = 3
        self.init = False
        self.spacer = 0

    def get_init_state(self):
        return self.init

    def event_manager(self):
        for event in pygame.event.get():
            # KEYBOARD EVENT
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    sys.exit()
                if event.key == pygame.K_LEFT:

                    self.move_cursor("LEFT")
                if event.key == pygame.K_RIGHT:
                    self.move_cursor("RIGHT")
                if event.key == pygame.K_RETURN:
                    self.check_user_selection(event)

            # MOUSE EVENT event.pos[0] = X event.pos[1] = Y
            if event.type == MOUSEMOTION:
                self.check_mouse_collision(event)
            if event.type == MOUSEBUTTONDOWN:
                self.check_user_selection(event)

    def load_image(self,filename):
        return pygame.image.load(filename).convert()

    def load_asset(self,screen):
        for i in range(len(self.initial)):
            # GET IMAGE IN THE SPRITESHEET
            imgRect = Rect(0,0,220,165)
            screen.blit(self.cursor_img,(self.initial.x, 
                                         self.initial.y),imgRect)
            self.initial.x += 222

    def draw_cursor(self,screen):
        imgRect = Rect(221,0,220,165)
        self.cursor_img.set_colorkey((0,0,0))
        screen.blit(self.cursor_img,(self.cursor_x[self.cursor_pos],
                                     self.initial.y),imgRect)

    def hide_selection(self,old_position):
        # HIDE THE SELECTION FOR UNSELECT SQUARE
        imgRect = Rect(442,0,220,165)
        self.cursor_img.set_colorkey((0,0,0))
        self.my_screen.blit(self.cursor_img,(self.cursor_x[old_position],
                                             self.initial.y),imgRect)

    def check_mouse_collision(self,event):
        for j in range(len(self.initial)):
            rect = Rect(self.cursor_x[j],390,220,220)
            if rect.colliderect(event.pos[0],event.pos[1],10,10):
                self.move_mouse_cursor(j)

    def move_cursor(self,direction):
        self.hide_selection(self.cursor_pos )
        if direction == "LEFT":
            if self.cursor_pos == 0:
                self.cursor_pos = 4
            self.cursor_pos -= 1
        else:
            if self.cursor_pos >= 3:
                self.cursor_pos = -1
            self.cursor_pos += 1

    def check_user_selection(self,event):
        print("RESULT: ", self.result)
        print("YOUR RESULT: " ,str(self.card_result[self.cursor_pos]),
              "ITEM: ", self.cursor_pos)
        if str(self.card_result[self.cursor_pos]) != str(self.result):
            print("BOOOOH")
        else: print("WELL DONE")

    def move_mouse_cursor(self,position):
        self.hide_selection(self.cursor_pos)
        self.cursor_pos = position

    # SHOW TEXT RESULT
    def show_result(self,screen,msg):
        self.result_img = self.font.render(msg,True,self.text_color)
        self.result_img_rect = (770,130,50,50)
        screen.blit(self.result_img,self.result_img_rect)

    # SHOW CALC TEXT 
    def show_calc(self,screen,calc,other_calc):
        #pdb.set_trace()
        self.string_calc = str(str(calc[0][0]) + " + " +str(calc[0][1]))
        self.calc_img = self.font.render(self.string_calc,True,
                                         self.text_color)

        self.calc_img_rect = (self.case_rect_x[self.selected_place_x],
                              450,50,60)
        # STORE RESULT IN VAR
        self.result = calc[0][0] + calc[0][1]
        # SELF.INIT TO AVOID LOOPING INFINITELY
        if self.init == False:   self.card_result[self.selected_place_x] = self.result
        self.show_other_calc(screen,other_calc)
        self.show_result(screen,str(self.result))
        screen.blit(self.calc_img,self.calc_img_rect)

    # SHOW THE 3 OTHERS CALC
    def show_other_calc(self,screen,calc):
        # SPACER IS EMPLACEMENT FOR OTHER CALC(0-3) 
        for i in calc:
            self.str_result = str(i[0]) + " + " + str(i[1])
          #  print("[SHOW_OTHER_CALC]STRING_RESULT: ", self.str_result)
            if self.spacer != 4:
                if self.spacer != self.selected_place_x:
                    if self.init == False:
                        self.card_result[self.spacer] = (i[0] + i[1])
                        self.result_img = self.font.render(str
                                                   (self.str_result),
                                                   True,self.text_color)
                    self.result_img_rect = (self.case_rect_x[self.spacer],
                                            450,50,60)
                    screen.blit(self.result_img,self.result_img_rect)
                if self.spacer <= 4: self.spacer += 1
        self.init = True

def main():
    pygame.init()
    screen = pygame.display.set_mode((1024,600))

    game = Game(screen)

    c = Calc(5)
    c.spawn()
    c.spawn_result()

    background_img = game.load_image('resources/pyMath1024_600.png')

    screen_rect = Rect(0,0,1024,600)

    screen.blit(background_img,screen_rect)
    game.load_asset(screen)

    while True:
        game.event_manager()

        #pdb.set_trace()
        game.show_calc(screen,c.get_result(),c.get_other_calc())
        #game.show_other_calc(screen,c.get_other_calc())
        game.draw_cursor(screen)

        pygame.display.update()

    pygame.quit()

main()
