#!/usr/bin/python3

import random


class Calc: 
    
    def __init__(self,max_nb):
        self.result = []
        self.max_nb = max_nb
        self.listCalc = []

    def spawn(self):
        i = 0
        while (i <= self.max_nb):
            # CALC TYPE IS TUPPLE 
            calc = (random.randint(0,9),random.randint(0,9))
            # CHECK IF THERE IS A DOUBLE
            if(Calc.check_double(calc[0]+calc[1]) == False):
                # LISTCALC IS LIST
                self.listCalc.append(list(calc))
                i += 1

    def search_double(list,element): 
        for i in range(len(list)):
            if list[i] == element:
                return True
        return False
    
    def check_double(sum):
        double = [] 
        '''
            RETURN TRUE IF THERE IS A DOUBLE  
            OTHERWISE ADD IT TO DOUBLE LIST  
        '''
        if Calc.search_double(double,sum):
            return True
        else: 
            double.append(sum)
            return False

    def spawn_random_selection():
        return random.randint(0,9)
    
    def show_calc(self):
        selected = Calc.spawn_random_selection()
        inc = 0

        for h in self.listCalc:
            print(inc,")", h[0], "+" , h[1], "=")

            if (inc == selected ):          
                self.result.append(selected)     # SELECT THE CORRECT RESULT 
                self.result.append(h[0])         # STORE FIRST RESULT x (x+y)
                self.result.append(h[1])         # STORE SECOND RESULT y (x+y)
            inc += 1                        # INC FOR USER INPUT

    def get_result(self):
        return self.result[0]

    def get_listCalc(self):
        return self.result

