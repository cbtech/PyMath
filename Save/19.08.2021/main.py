#!/usr/bin/python3

import os
import pygame
from randomNumber import Calc
from pygame.locals import *
import pygame.font
import sys

class Game:
    def __init__(self):
        self.case_list = [] 
        self.text_color = (66,66,66)
        self.font = pygame.font.SysFont(None, 60)

    def event_manager(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE :
                    sys.exit()

    def load_image(self,filename):
        return pygame.image.load(filename).convert()

    def load_asset(self,screen,img):
        initial = Rect(70,390,222,166)
        for i in range(0,4):
            self.case_list.append(Rect(initial.x,initial.y,initial.width,initial.height))
            screen.blit(img,self.case_list[i])
            initial.x += 222

    def show_result(self,screen,msg):
        self.result_img = self.font.render(msg,True,self.text_color)
        self.result_img_rect = (440,280,50,50)
        screen.blit(self.result_img,self.result_img_rect)

    def show_calc(self,screen,calc_m1,calc_m2):
        self.string_calc = calc_m1 + " + " + calc_m2  
        self.calc_img = self.font.render(self.string_calc,True,self.text_color)
        self.calc_img_rect = (790,450,50,60)
        result = int(calc_m1) + int(calc_m2)
        self.show_result(screen,str(result))
        screen.blit(self.calc_img,self.calc_img_rect)

def main():
    pygame.init()
    screen = pygame.display.set_mode((1024,600))

    game = Game()

    c = Calc(10)
    c.spawn()

    background_img = game.load_image('resources/pyMath1024_600.png')
    case_img = game.load_image('resources/Asset.png')


    screen_rect = Rect(0,0,1024,600)

    screen.blit(background_img,screen_rect)
    game.load_asset(screen,case_img)

    while True:
        game.event_manager()

        c.show_calc()
        game.show_calc(screen,str(c.get_listCalc()[1]),str(c.get_listCalc()[2]))
        
        pygame.display.update()

    pygame.quit()

main()
