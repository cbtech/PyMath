#!/usr/local/bin/python3.8

import os
import pygame
from randomNumber import Calc
import random
from pygame.locals import *
import pygame.font
import sys
import pdb
import time

class Game:
    def __init__(self,screen):
        self.initial = Rect(70,390,220,166)
        self.my_screen = screen
        self.cursor_img = self.load_image(
                          'resources/Asset.png') 
        self.cursor_x = [70,290,513,735]
        self.cursor_pos = 0
        self.case_rect_x = [135,350,575,790]
        self.card_result = [0,0,0,0]
        self.selected_place_x = random.randint(0,3)
        self.text_color = (66,66,66)
        self.font = pygame.font.SysFont(None, 60)
        self.result = 0
        self.result_msg = " " 
        self.max_calc = 3
        self.init = False
        self.spacer = 0
        self.is_winning = False
    
    def get_init_state(self):
        return self.init
    
    def is_done(self):
        return self.is_winning
    
    def set_is_winning(self,state):
        self.is_winning = False

    def event_manager(self):
        for event in pygame.event.get():
            # KEYBOARD EVENT
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    sys.exit()
                if event.key == pygame.K_LEFT:
                    self.move_cursor("LEFT")
                if event.key == pygame.K_RIGHT:
                    self.move_cursor("RIGHT")
                if event.key == pygame.K_RETURN:
                    self.check_user_selection(event)

            if event.type == MOUSEMOTION:
                self.check_mouse_collision(event)
            if event.type == MOUSEBUTTONDOWN:
                self.check_user_selection(event)

    def load_image(self,filename):
        return pygame.image.load(filename).convert()

# GET IMAGE IN THE SPRITESHEET
    def load_asset(self,screen):
        #pdb.set_trace()
        for i in range(len(self.initial)):
            imgRect = Rect(0,0,220,165)
            screen.blit(self.cursor_img,(
                    self.initial.x,self.initial.y),
                    imgRect)
            self.initial.x += 222

    def draw_cursor(self,screen):
        imgRect = Rect(221,0,220,165)
        self.cursor_img.set_colorkey((0,0,0))
        screen.blit(self.cursor_img,
            (self.cursor_x[self.cursor_pos],
            self.initial.y),imgRect)

# HIDE THE SELECTION FOR UNSELECT SQUARE
    def hide_selection(self,old_position):
        imgRect = Rect(442,0,220,165)
        self.cursor_img.set_colorkey((0,0,0))
        self.my_screen.blit(self.cursor_img,(
           self.cursor_x[old_position],
           self.initial.y),imgRect)

    def check_mouse_collision(self,event):
        for j in range(len(self.initial)):
            rect = Rect(self.cursor_x[j],
                        390,220,220)
            if rect.colliderect(event.pos[0],
                         event.pos[1],10,10):
                self.move_mouse_cursor(j)

    def move_cursor(self,direction):
        self.hide_selection(self.cursor_pos )
        if direction == "LEFT":
            if self.cursor_pos == 0:
                self.cursor_pos = 4
            self.cursor_pos -= 1
        else:
            if self.cursor_pos >= 3:
                self.cursor_pos = -1
            self.cursor_pos += 1

    def move_mouse_cursor(self,position):
        self.hide_selection(self.cursor_pos)
        self.cursor_pos = position

    def check_user_selection(self,event):
        if (str(self.card_result[self.cursor_pos])     
            != str(self.result)):
            self.draw_BOOOH()
        else:
            self.draw_GOOD_JOB()
        
        pygame.display.update()
        time.sleep(2)
        self.is_winning = True
        
    def clear_zone(self,screen):
        # CLEAR BOARD AND SQUARE(WHITE)
        screen.fill(pygame.Color(255,255,255),(770,130,50,50))
        # CLEAR GOOD/BOOH(YELLOW)
        screen.fill(pygame.Color(255,194,0),(100,130,220,40))

    def draw_GOOD_JOB(self):
        self.win_text = self.font.render(
                "GOOD JOB", True,self.text_color)
        self.win_rect = (100,130,50,50)
        self.my_screen.blit(self.win_text,
                            self.win_rect)
        #time.sleep(3);

    def draw_BOOOH(self):
        self.win_text = self.font.render(
            "BOOHHH",True,self.text_color)
        self.win_rect = (100,130,50,50)
        self.my_screen.blit(self.win_text,
                            self.win_rect)

    def draw_score(self):
            pass


    def reset(self):
        self.initial = Rect(70,390,220,166)
        self.init = False
        self.spacer = 0

# SHOW TEXT RESULT
    def show_result(self,screen,msg):
        self.result_msg = msg
        self.result_img = self.font.render(self.result_msg,True,self.text_color)
        self.result_img_rect = (770,130,50,50)
        screen.blit(self.result_img,self.result_img_rect)

# SHOW CALC TEXT 
    def show_calc(self,screen,calc,other_calc):
        self.string_calc = str(str(calc[0][0]) + " + " +str(calc[0][1]))
        self.calc_img = self.font.render(self.string_calc,True,
                                         self.text_color)

        self.calc_img_rect = (self.case_rect_x[self.selected_place_x],
                              450,50,60)
        # STORE RESULT IN VAR
        self.result = calc[0][0] + calc[0][1]
        # SELF.INIT TO AVOID LOOPING INFINITELY
        if self.init == False:   self.card_result[self.selected_place_x] = self.result
        self.show_other_calc(screen,other_calc)
        self.show_result(screen,str(self.result))
        screen.blit(self.calc_img,self.calc_img_rect)

# SHOW 3 OTHERS CALC
    def show_other_calc(self,screen,calc):
        # SPACER IS EMPLACEMENT FOR OTHER CALC(0-3) 
        for i in calc:
            self.str_result = str(i[0]) + " + " + str(i[1])
            if self.spacer != 4:
                if self.spacer != self.selected_place_x:
                    if self.init == False:
                        self.card_result[self.spacer] = (i[0] + i[1])
                        self.result_img = self.font.render(str
                                                   (self.str_result),
                                                   True,self.text_color)
                    self.result_img_rect = (self.case_rect_x[self.spacer],
                                            450,50,60)
                    screen.blit(self.result_img,self.result_img_rect)
                if self.spacer <= 4: self.spacer += 1
        self.init = True

def respawn(screen,game):
    # SPAWN 5 CALC IF THERE IS DOUBLE
    c = Calc(5)
    c.clear_list()
    c.spawn()
    c.spawn_result()

    game.load_asset(screen)
    game.show_calc(screen,c.get_result(),c.get_other_calc())
    return c

def main():
    pygame.init()
    screen = pygame.display.set_mode((1024,600))

    game = Game(screen)

    background_img = game.load_image('resources/pyMath1024_600.png')
    screen_rect = Rect(0,0,1024,600)
    screen.blit(background_img,screen_rect)

    c = respawn(screen,game)
    while True:
        game.event_manager()
        game.draw_cursor(screen)
        
        if (game.is_done() == True):
            game.reset()
            game.clear_zone(screen)
            # REDRAW SCREEN
            respawn(screen,game)
            game.is_winning = False
        
        pygame.display.update()
    pygame.quit()

main()
